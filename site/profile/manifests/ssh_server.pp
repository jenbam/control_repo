class profile::ssh_server {
        package {'openssh-server':
                ensure => present,  
        }
        service {'sshd':
                ensure => 'running',
                enable => 'true',
        }
        ssh_authorized_key {'root@master.puppet.vm':
                ensure => 'present',
                user => 'root',
                type => 'ssh-rsa',
                key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDUaL+KA3MBuczdpGfDYsu2yOV3juCZV/UYjY/huhQzvoMh6YyHObo0J/PCs2t+8wEouH1QxMm2M/x7PgCxYRn0Guf5eVVkBIROdl5g6Fp2U7rLOO79kg469arK20TwkCyXJTChSjHPsaRpmTAr9h1eQr9G3H/FlP314H5GKEvj8t3+ivniTcaeXIzhvVgfTheOaCVBVlURztI5tuJ9d+vCIT6+VYWN289HSjoDTjqGRoW680HsxyQ+BjHpO0FTTvI0EneEg3F7mLnIE+/A3tCb6p5hrbDEmWPqD9qTcGq2GK9dgaDzHA1ObIEDPNIQmEJcR2rxCcPVOABGKuHlVm1x',
        }
}